package iamdev.me.ytds.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import iamdev.me.ytds.entity.DownloadRecord;
import iamdev.me.ytds.mapper.DownloadRecordMapper;
import iamdev.me.ytds.service.IDownloadRecordService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author zxc
 * @since 2018-06-22
 */
@Service
public class DownloadRecordServiceImpl extends
    ServiceImpl<DownloadRecordMapper, DownloadRecord> implements IDownloadRecordService {

}
